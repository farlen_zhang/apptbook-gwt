package edu.pdx.cs410J.fzhang.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import edu.pdx.cs410J.fzhang.client.*;


import java.io.IOException;
import java.util.Date;


/**
 * The server-side implementation of the appointment book application
 */
public class AppointmentBookServiceImpl extends RemoteServiceServlet implements AppointmentBookService {
    private AppointmentBook apptbook;

    /**
     * Adds new <code>Appointment</code> to an <code>AppointmentBook</code>, creates one if doesn't exist
     *
     * @param owner       the owner of the <code>AppointmentBook</code>
     * @param description the description of the <code>Appointment</code>
     * @param beginTime   the begin time of the <code>Appointment</code>
     * @param endTime     the end time of the <code>Appointment</code>
     * @return the <code>AppointmentBook</code>
     * @throws IOException     if failed to write the <code>AppointmentBook</code> back to the text file
     * @throws ParserException if error parsing the text file
     */
    @Override
    public AppointmentBook createAppointmentBook(String owner, String description, Date beginTime, Date endTime) throws IOException, ParserException {
        try {
            apptbook = (AppointmentBook) new TextParser(getFilePath(owner)).parse();
        } catch (IOException e) {
            // if appointment book doesn't exist, create a new one
            apptbook = new AppointmentBook(owner);
        }
        Appointment appt = new Appointment(description, beginTime, endTime);
        apptbook.addAppointment(appt);

        try {
            new TextDumper(getFilePath(owner)).dump(apptbook);
        } catch (IOException e) {
            // if failed to write to file
            throw new IOException("Failed to write to file");
        }
        return apptbook;
    }

    /**
     * file path getter
     * @param owner the owner of the <code>AppointmentBook</code>
     * @return  "owner.txt" as a <code>String</code>
     */
    private String getFilePath(String owner) {
        return owner + ".txt";
    }

    /**
     * Pretty prints an <code>AppointmentBook</code>
     *
     * @param owner the owner of the <code>AppointmentBook</code>
     * @return the <code>AppointmentBook</code>
     * @throws IOException     if appointment book file does not exist
     * @throws ParserException if error parsing the text file
     */
    @Override
    public AppointmentBook prettyPrintAppointmentBook(String owner) throws IOException, ParserException {
        try {
            apptbook = (AppointmentBook) new TextParser(getFilePath(owner)).parse();
        } catch (IOException e) {
            // if appointment book doesn't exist, create a new one
            throw new IOException("Appointment book does not exist");
        }
        return apptbook;
    }

    /**
     * Searches for appointments during a specific time period and pretty prints the search results
     *
     * @param owner     the owner of the <code>AppointmentBook</code>
     * @param beginTime the begin time of searching period
     * @param endTime   the end time of searching period
     * @return the search results as a <code>String</code>
     * @throws ParserException if appointment book does not exist
     * @throws IOException     if error parsing the text file
     */
    @Override
    public String searchForAppointments(String owner, Date beginTime, Date endTime) throws ParserException, IOException {
        try {
            apptbook = (AppointmentBook) new TextParser(getFilePath(owner)).parse();
        } catch (IOException e) {
            // if appointment book doesn't exist, create a new one
            throw new IOException("Appointment book does not exist");
        }
        StringBuilder sb = new StringBuilder();
        sb.append(owner).append("\n");
        for (Appointment appt : apptbook.getAppointments()) {
            if (!appt.getBeginTime().before(beginTime) &&
                    !appt.getEndTime().after(endTime)) {
                sb.append(appt.dumpPretty());
            }
        }
        return sb.toString();
    }

    /**
     * Logs uncaught exceptions encountered while invoking a service method
     * The exception is still sent to the client, but this logging gets you the stack trace on the server side.
     *
     * @param unhandled  the <code>Exception</code>
     */
    @Override
    protected void doUnexpectedFailure(Throwable unhandled) {
        unhandled.printStackTrace(System.err);
        super.doUnexpectedFailure(unhandled);
    }
}
