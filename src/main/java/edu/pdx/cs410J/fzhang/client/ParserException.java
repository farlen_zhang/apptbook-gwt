package edu.pdx.cs410J.fzhang.client;

/**
 * A <code>ParserException</code> is thrown when a file or other data
 * source is being parsed and it is decided that the source is
 * malformatted
 */
public class ParserException extends edu.pdx.cs410J.ParserException {
    /**
     * Creates a new <code>ParserException</code> with a predefined
     * descriptive message.
     */
    public ParserException() {
        super("Eroor parsing the appointment book file");
    }

    /**
     * Creates a new <code>ParserException</code> with a given
     * descriptive message.
     *
     * @param description  the cuase of the exception
     */
    public ParserException(String description) {
        super(description);
    }

    /**
     * Creates a new <code>ParserException</code> that was caused by
     * another exception.
     *
     * @param description  the cuase of the exception
     * @param cause  the cause that is a <code>Throwable</code>
     */
    public ParserException(String description, Throwable cause) {
        super(description, cause);
    }
}
