package edu.pdx.cs410J.fzhang.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

import java.util.Date;

/**
 * The client-side interface to the AppointmentBook service
 */
public interface AppointmentBookServiceAsync {

  /**
   * Adds new <code>Appointment</code> to an <code>AppointmentBook</code>, creates one if doesn't exist
   * @param owner  the owner of the <code>AppointmentBook</code>
   * @param description the description of the <code>Appointment</code>
   * @param beginTime the begin time of the <code>Appointment</code>
   * @param endTime the end time of the <code>Appointment</code>
     * @param async a <code>AsyncCallback </code> object
     */
  void createAppointmentBook(String owner, String description, Date beginTime, Date endTime, AsyncCallback<AppointmentBook> async);

  /**
   * Pretty prints an <code>AppointmentBook</code>
   * @param owner the owner of the <code>AppointmentBook</code>
   * @param async a <code>AsyncCallback </code> object
     */
  void prettyPrintAppointmentBook(String owner, AsyncCallback<AppointmentBook> async);

  /**
   * Searches for appointments during a specific time period and pretty prints the search results
   * @param owner the owner of the <code>AppointmentBook</code>
   * @param beginTime the begin time of searching period
   * @param endTime the end time of searching period
   * @param async a <code>AsyncCallback </code> object
     */
  void searchForAppointments(String owner, Date beginTime, Date endTime, AsyncCallback<String> async);
}
