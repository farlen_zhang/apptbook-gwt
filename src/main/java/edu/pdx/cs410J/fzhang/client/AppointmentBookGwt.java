package edu.pdx.cs410J.fzhang.client;

import com.google.common.annotations.VisibleForTesting;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Style.Unit;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.*;


import static edu.pdx.cs410J.fzhang.client.Utils.parseDateTime;

/**
 * A GWT class that makes sure that we can send requests to server and server can send back requested objects back to us
 */
public class AppointmentBookGwt implements EntryPoint {
    private final Alerter alerter;

    /**
     * Constructor that creates a new <code>Alerter </code> and calls the other constructor that takes an <code>Alerter</code>
     */
    public AppointmentBookGwt() {
        this(new Alerter() {
            @Override
            public void alert(String message) {
                Window.alert(message);
            }
        });
    }

    /**
     * Constructor that takes an <code>Alerter</code> and set the alerter field
     *
     * @param alerter an <code>Alerter</code>
     */
    @VisibleForTesting
    AppointmentBookGwt(Alerter alerter) {
        this.alerter = alerter;
    }

    /**
     * The entry point method, called automatically by loading the apptbook module
     */
    @Override
    public void onModuleLoad() {
        GWT.setUncaughtExceptionHandler(new GWT.UncaughtExceptionHandler() {
            /**
             * Alerts the user to uncaught exceptions in client-side code.
             * @param throwable the <code>Exception</code>
             */
            @Override
            public void onUncaughtException(Throwable throwable) {
                alerter.alert(throwable.getMessage());
            }
        });
        HorizontalPanel hPanel = new HorizontalPanel();
        hPanel.setBorderWidth(1);
        hPanel.setSpacing(10);
        hPanel.add(buildMenu());
        hPanel.add(buildCreatePanel());
        hPanel.add(buildPrettyPrintPanel());
        hPanel.add(buildSearchPanel());
        RootPanel.get().add(hPanel);
    }

    /**
     * Builds the Help panel
     *
     * @return the panel
     */
    private Panel buildMenu() {
        DockLayoutPanel pn = new DockLayoutPanel(Unit.EM);
        pn.addNorth(new HTML("<h3>Help</h3>"), 4);
        Button button = new Button("README");
        pn.addSouth(button, 2);
        pn.add(new Label("Click the README button for usage"));
        pn.setSize("25em", "16em");

        final StringBuilder readMe = new StringBuilder();
        readMe.append("A Rich Internet Application for an Appointment Book:")
                .append("\n")
                .append("\n")
                .append("To create an appointment book, give Owner, Description, Begin and End time of the appointment, ")
                .append("\n")
                .append("and then click the Create button")
                .append("\n")
                .append("\n")
                .append("To print out an appointment book, give the Owner and click the Pretty Print button")
                .append("\n")
                .append("\n")
                .append("To search for appointments that occured between a specific time period, ")
                .append("\n")
                .append("give the Owner, Begin and End time and click the Search button");

        button.addClickHandler(new ClickHandler() {
            /**
             * Displays the usage of this application on clicking of the README button
             * @param clickEvent
             */
            @Override
            public void onClick(ClickEvent clickEvent) {
                alerter.alert(readMe.toString());
            }
        });
        return pn;
    }


    /**
     * Build the create appointment book panel
     *
     * @return the panel
     */
    private Panel buildCreatePanel() {
        DockLayoutPanel pn = new DockLayoutPanel(Unit.EM);
        pn.addNorth(new HTML("<h3>Create a New Appointment</h3>"), 4);
        Button button = new Button("Create");
        pn.addSouth(button, 2);

        VerticalPanel labels = new VerticalPanel();
        labels.add(new Label("Owner"));
        labels.add(new Label("Description"));
        labels.add(new Label("Begin"));
        labels.add(new Label("End"));
        labels.setSpacing(3);
        pn.addWest(labels, 6);

        VerticalPanel textBoxes = new VerticalPanel();
        final TextBox ownerField = new TextBox();
        textBoxes.add(ownerField);
        final TextBox descField = new TextBox();
        textBoxes.add(descField);
        final TextBox beginField = new TextBox();
        textBoxes.add(beginField);
        final TextBox endField = new TextBox();
        textBoxes.add(endField);
        pn.add(textBoxes);

        pn.setSize("25em", "16em");

        button.addClickHandler(new ClickHandler() {
            /**
             * Sends RPC to the createAppointmentBook method on server side on clicking of the Create button
             * @param clickEvent
             */
            @Override
            public void onClick(ClickEvent clickEvent) {
                AppointmentBookServiceAsync async = GWT.create(AppointmentBookService.class);
                try {
                    async.createAppointmentBook(ownerField.getText(), descField.getText(),
                            parseDateTime(beginField.getText()), parseDateTime(endField.getText()),
                            new AsyncCallback<AppointmentBook>() {
                                /**
                                 * Prints out the error message if failed
                                 * @param ex  the <code>Exception</code> sent back from server
                                 */
                                @Override
                                public void onFailure(Throwable ex) {
                                    alerter.alert(ex.getMessage());
                                }

                                /**
                                 * Prints the appointment book's toString method
                                 * @param apptbook  the <code>AppointmentBook</code>
                                 */
                                @Override
                                public void onSuccess(AppointmentBook apptbook) {
                                    alerter.alert(
                                            "Successfully created appointment book: "
                                                    + apptbook);
                                }
                            });
                } catch (IllegalArgumentException e) {
                    alerter.alert("Invalid Date or Time format");
                }
            }
        });
        return pn;
    }

    /**
     * Build the pretty print appointments panel
     *
     * @return the panel
     */
    private Panel buildPrettyPrintPanel() {
        DockLayoutPanel pn = new DockLayoutPanel(Unit.EM);
        pn.addNorth(new HTML("<h3>Pretty Print an Appointment Book</h3>"), 4);
        Button button = new Button("Pretty Print");
        pn.addSouth(button, 2);
        pn.addWest(new Label("Owner"), 6);
        final TextBox ownerField = new TextBox();
        ownerField.setHeight("2em");
        pn.add(ownerField);
        pn.setSize("25em", "16em");

        button.addClickHandler(new ClickHandler() {
            /**
             * Sends RPC to the prettyPrintAppointmentBook method on server side on clicking of the Pretty Print button
             * @param clickEvent
             */
            @Override
            public void onClick(ClickEvent clickEvent) {
                AppointmentBookServiceAsync async = GWT.create(AppointmentBookService.class);
                async.prettyPrintAppointmentBook(ownerField.getText(), new AsyncCallback<AppointmentBook>() {

                    /**
                     * Prints out the error message if failed
                     * @param throwable  the <code>Exception</code> sent back from server
                     */
                    @Override
                    public void onFailure(Throwable throwable) {
                        alerter.alert(throwable.getMessage());
                    }

                    /**
                     * Pretty prints the appointment book
                     * @param appointmentBook  the <code>AppointmentBook</code>
                     */
                    @Override
                    public void onSuccess(AppointmentBook appointmentBook) {
                        alerter.alert(appointmentBook.dump(true));
                    }
                });
            }
        });
        return pn;
    }

    /**
     * Builds the search for appointments panel
     *
     * @return the panel
     */
    private Panel buildSearchPanel() {
        DockLayoutPanel pn = new DockLayoutPanel(Unit.EM);
        pn.addNorth(
                new HTML(
                        "<h3>Search for Appointments in an Appointment Book</h3>"),
                4);
        Button button = new Button("Search");
        pn.addSouth(button, 2);

        VerticalPanel labels = new VerticalPanel();
        labels.add(new Label("Owner"));
        labels.add(new Label("Begin"));
        labels.add(new Label("End"));
        labels.setSpacing(3);
        pn.addWest(labels, 6);

        VerticalPanel textBoxes = new VerticalPanel();
        final TextBox ownerField = new TextBox();
        textBoxes.add(ownerField);
        final TextBox beginField = new TextBox();
        textBoxes.add(beginField);
        final TextBox endField = new TextBox();
        textBoxes.add(endField);
        pn.add(textBoxes);

        pn.setSize("30em", "16em");

        button.addClickHandler(new ClickHandler() {
            /**
             * Sends RPC to the searchForAppointments method on server side
             * @param clickEvent
             */
            @Override
            public void onClick(ClickEvent clickEvent) {
                AppointmentBookServiceAsync async = GWT.create(AppointmentBookService.class);
                try {
                    async.searchForAppointments(ownerField.getText(), parseDateTime(beginField.getText()), parseDateTime(endField.getText()), new AsyncCallback<String>() {
                        /**
                         * Prints out the error message if failed
                         * @param throwable  the <code>Exception</code>
                         */
                        @Override
                        public void onFailure(Throwable throwable) {
                            alerter.alert(throwable.getMessage());
                        }

                        /**
                         * Pretty prints the appointments that meet the searching criteria
                         * @param s  the pretty format of the appointments as a <code>String</code>
                         */
                        @Override
                        public void onSuccess(String s) {
                            alerter.alert(s);

                        }
                    });
                } catch (IllegalArgumentException e) {
                    alerter.alert("Invalid Date or Time format");
                }
            }
        });
        return pn;
    }


    /**
     * The Alerter interface
     */
    @VisibleForTesting
    interface Alerter {
        void alert(String message);
    }

}
