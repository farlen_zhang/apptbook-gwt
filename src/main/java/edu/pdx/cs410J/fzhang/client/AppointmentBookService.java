package edu.pdx.cs410J.fzhang.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

import java.io.IOException;
import java.util.Date;

/**
 * GWT remote services
 */
@RemoteServiceRelativePath("appointments")
public interface AppointmentBookService extends RemoteService {

  /**
   * Adds new <code>Appointment</code> to an <code>AppointmentBook</code>, creates one if doesn't exist
   * @param owner  the owner of the <code>AppointmentBook</code>
   * @param description  the description of the <code>Appointment</code>
   * @param beginTime  the begin time of the <code>Appointment</code>
   * @param endTime  the end time of the <code>Appointment</code>
   * @return  the <code>AppointmentBook</code>
   * @throws IOException  if failed to write the <code>AppointmentBook</code> back to the text file
     * @throws ParserException  if error parsing the text file
     */
  public AppointmentBook createAppointmentBook(String owner, String description, Date beginTime, Date endTime) throws IOException, ParserException;

  /**
   * Pretty prints an <code>AppointmentBook</code>
   * @param owner  the owner of the <code>AppointmentBook</code>
   * @return  the <code>AppointmentBook</code>
   * @throws IOException  if appointment book file does not exist
   * @throws ParserException  if error parsing the text file
     */
  public AppointmentBook prettyPrintAppointmentBook(String owner) throws IOException, ParserException;

  /**
   * Searches for appointments during a specific time period and pretty prints the search results
   * @param owner  the owner of the <code>AppointmentBook</code>
   * @param beginTime  the begin time of searching period
   * @param endTime  the end time of searching period
   * @return  the search results as a <code>String</code>
   * @throws IOException  if appointment book does not exist
   * @throws ParserException  if error parsing the text file
     */
  public String searchForAppointments(String owner, Date beginTime, Date endTime) throws IOException, ParserException;

}
