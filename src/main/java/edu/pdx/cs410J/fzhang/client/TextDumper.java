package edu.pdx.cs410J.fzhang.client;

import edu.pdx.cs410J.AbstractAppointmentBook;
import edu.pdx.cs410J.AppointmentBookDumper;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Dumps the contents of an <code>AppointmentBook</code>
 * (including <code>Appointment</code>s) to a text file
 */
public class TextDumper implements AppointmentBookDumper {
    private String filePath;

    /**
     * Constructor of class <code>TextDumper</code>,
     * sets the field <code>filePath</code>
     *
     * @param filePath the path of the text file as a <code>String</code>
     */
    public TextDumper(String filePath) {
        this.filePath = filePath;
    }

    /**
     * This method dumps the contents of an <code>AppointmentBook</code>
     * to a text file as specified by the field <code>filePath</code>
     *
     * @param abstractAppointmentBook an <code>AbstractAppointmentBook</code>
     * @throws IOException if fails to write the the text file
     */
    @Override
    public void dump(AbstractAppointmentBook abstractAppointmentBook) throws IOException {
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(filePath));
        writer.write(((AppointmentBook) abstractAppointmentBook)
                .dump(false));
        writer.close();
    }
}
