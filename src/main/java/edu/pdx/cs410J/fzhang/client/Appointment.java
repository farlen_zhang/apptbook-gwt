package edu.pdx.cs410J.fzhang.client;

import edu.pdx.cs410J.AbstractAppointment;

import java.util.Date;

import static edu.pdx.cs410J.fzhang.client.Utils.*;

/**
 * Represents an <code>Appointment</code>,
 * an <code>Appointment</code> has some description, begins at a given time and ends at a given time
 */
public class Appointment extends AbstractAppointment implements Comparable<Appointment> {

    private String description;
    private Date beginDateTime;
    private Date endDateTime;


    /**
     * Constructor of class <code>Appointment</code>
     */
    public Appointment() {
        this("Default Description", new Date(), new Date());
    }


    /**
     * Constructor of class <code>Appointment</code>
     * Sets the <code>description</code>, <code>beginDate</code>, <code>beginTime</code>, <code>endDate</code> and <code>endTime</code>
     *
     * @param description   the description of this <code>Appointment</code> as a <code>String</code>
     * @param beginDateTime the begin date of this <code>Appointment</code> as a <code>Date</code>
     * @param endDateTime   the end date of this <code>Appointment</code> as a <code>Date</code>
     */
    public Appointment(String description, Date beginDateTime, Date endDateTime) {
        this.description = description;
        this.beginDateTime = beginDateTime;
        this.endDateTime = endDateTime;
    }

    /**
     * Gets the begin time of the <code>Appointment</code>
     *
     * @return the begin time as a <code>Date</code>
     */
    @Override
    public Date getBeginTime() {
        return beginDateTime;
    }

    /**
     * Gets the end time of the <code>Appointment</code>
     *
     * @return the end time as a <code>Date</code>
     */
    @Override
    public Date getEndTime() {
        return endDateTime;
    }

    /**
     * Gets the begin date and begin time of the <code>Appointment</code>
     *
     * @return the concatenation of <code>beginDate</code> and <code>beginTime</code> as a <code>String</code>
     */
    @Override
    public String getBeginTimeString() {
        return formatDateTime(beginDateTime);
    }

    /**
     * Gets the end date and end time of the <code>Appointment</code>
     *
     * @return the concatenation of <code>endDate</code> and <code>endTime</code> as a <code>String</code>
     */
    @Override
    public String getEndTimeString() { return formatDateTime(endDateTime); }

    /**
     * Gets the description of the <code>Appointment</code>
     *
     * @return the <code>description</code> of the <code>Appointment</code>
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * This method presents the <code>String</code> representation of
     * an <code>Appointment</code> in plain format
     *
     * @return the concatenation of description, begin time and end time of
     * the <code>Appointment</code> as a <code>String</code>
     */
    String dumpPlain() {
        StringBuilder sb = new StringBuilder();
        sb
                .append(getDescription())
                .append("\n")
                .append(getBeginTimeString())
                .append("\n")
                .append(getEndTimeString())
                .append("\n");
        return sb.toString();
    }

    /**
     * This method presents the <code>String</code> representation of
     * an <code>Appointment</code> in pretty forat
     *
     * @return the concatenation of description, begin time, end time and duration of
     * the <code>Appointment</code> as a <code>String</code>
     */
    public String dumpPretty() {
        StringBuilder sb = new StringBuilder();
        sb
                .append("\n")
                .append(getDescription())
                .append("\n")
                .append(prettyFormatDateTime(beginDateTime))
                .append("\n")
                .append(prettyFormatDateTime(endDateTime))
                .append("\n")
                .append("Duration: " + getDuration(beginDateTime, endDateTime) + " minutes.")
                .append("\n");
        return sb.toString();
    }

    /**
     * This method gets the duration of an <code>Appointment</code>
     *
     * @param beginDateTime begin time of the <code>Appointment</code> as a <code>Date</code>
     * @param endDateTime   end time of the <code>Appointment</code> as a <code>Date</code>
     * @return the duration of the <code>Appointment</code> as a <code>long</code>
     */
    private long getDuration(Date beginDateTime, Date endDateTime) {
        long diffInMillies = endDateTime.getTime() - beginDateTime.getTime();
        return diffInMillies / 1000 / 60;
    }

    /**
     * Compare two appointments first by begin time, then by end time
     * then by description
     *
     * @param o an <code>Appointmewnt</code>
     * @return the compare result as an <code>int</code>
     */
    @Override
    public int compareTo(Appointment o) {
        int compareResult = beginDateTime.compareTo(o.beginDateTime);
        if (compareResult != 0) {
            return compareResult;
        } else {
            compareResult = endDateTime.compareTo(o.endDateTime);
            if (compareResult != 0) {
                return compareResult;
            } else {
                compareResult = description.compareTo(o.description);
                return compareResult;
            }
        }
    }
}
