package edu.pdx.cs410J.fzhang.client;

import edu.pdx.cs410J.AbstractAppointmentBook;

import java.util.Collection;
import java.util.TreeSet;

/**
 * Represents an <code>AppointmentBook</code>,
 * an <code>AppointmentBook</code> belongs to a particular owner and consists of multiple <code>Appointments</code>
 */
public class AppointmentBook extends AbstractAppointmentBook<Appointment> {
    private String owner;
    private TreeSet<Appointment> appointments = new TreeSet<>();

    /**
     * A constructor of class <code>AppointmentBook</code> as required by Serializable
     */
    public AppointmentBook() {
    }

    /**
     * A constructor of class <code>AppointmentBook</code>, it sets the <code>owner</code> of this <code>AppointmentBook</code>
     *
     * @param owner the owner of the <code>AppointmentBook</code> as a <code>String</code>
     */
    public AppointmentBook(String owner) {
        this.owner = owner;
    }

    /**
     * Gets the owner of the <code>AppointmentBook</code>
     *
     * @return the <code>owner</code>
     */
    @Override
    public String getOwnerName() {
        return owner;
    }

    /**
     * Gets all the <code>Appointment</code>s in this <code>AppointmentBook</code>
     *
     * @return the list of <code>Appointment</code>s
     */
    @Override
    public Collection<Appointment> getAppointments() {
        return appointments;
    }

    /**
     * Adds new <code>Appointment</code> into the <code>AppointmentBook</code>
     *
     * @param appointment an <code>Appointment</code> object
     */
    @Override
    public void addAppointment(Appointment appointment) {
        appointments.add(appointment);
    }

    /**
     * This method presents the <code>String</code> representation of  an <code>AppointmentBook</code>
     *
     * @param shouldPrettyPrint  the flag of whether there's -pretty option or not
     * @return  the owner name and information of all <code>Appointment</code> as a <code>String</code>
     */
    public String dump(boolean shouldPrettyPrint) {
        StringBuilder sb = new StringBuilder();
        sb.append(owner).append("\n");
        for (Appointment appointment : appointments) {
            if (shouldPrettyPrint) {
                sb.append(appointment.dumpPretty());
            } else {
                sb.append(appointment.dumpPlain());
            }
        }
        return sb.toString();
    }
}
