package edu.pdx.cs410J.fzhang.client;

import com.google.gwt.i18n.shared.DateTimeFormat;
import com.google.gwt.i18n.shared.DefaultDateTimeFormatInfo;

import java.util.Date;

/**
 * The utility class
 */
public class Utils {

    //static final SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
    //static final DateTimeFormat format = DateTimeFormat.getFormat("MM/dd/yyyy hh:mm a");

    private static final DateTimeFormat format;
    private static final DateTimeFormat prettyFmt;

    static {
        // TODO why do we do this?
        // http://stackoverflow.com/questions/10284564/gwt-use-datetimeformat-on-client-and-simpledateformat-on-server
        // http://svenbuschbeck.net/wordpress/2012/09/using-gwts-datetimeformat-in-server-side-code/
        DefaultDateTimeFormatInfo info = new DefaultDateTimeFormatInfo();
        prettyFmt = new DateTimeFormat("yyyy-MM-dd EEE hh:mm aa", info) {
        };
        format = new DateTimeFormat("MM/dd/yyyy hh:mm a", info) {
        };
    }


    /**
     * Validates and parses the Date and Time arguments
     *
     * @param dateTime  date & time as a <code>String</code>
     * @return  the parsed date and time in <code>SHORT</code> format
     * @throws IllegalArgumentException  if parsing failed
     */
    public static Date parseDateTime(String dateTime) {
        // format.parse method throws IllegalArgument exception
        return format.parse(dateTime);
    }

    /**
     * Convert a <code>Date</code> to <code>String</code> using plain format
     * @param dateTime a <code>Date</code>
     * @return the <code>String</code> that the <code>Date</code> formatted to
     */
    public static String formatDateTime(Date dateTime) {
        return format.format(dateTime);
    }

    /**
     * Convert a <code>Date</code> to <code>String</code> using pretty format
     * @param dateTime a <code>Date</code>
     * @return the <code>String</code> that the <code>Date</code> formatted to
     */
    public static String prettyFormatDateTime(Date dateTime) {
        return prettyFmt.format(dateTime);
    }

}
