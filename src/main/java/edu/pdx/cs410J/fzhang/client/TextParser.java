package edu.pdx.cs410J.fzhang.client;

import edu.pdx.cs410J.AbstractAppointmentBook;
import edu.pdx.cs410J.AppointmentBookParser;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;

import static edu.pdx.cs410J.fzhang.client.Utils.parseDateTime;

/**
 * Reads the contents of a text file and creates an <code>AppointmentBook</code>
 * with its associated <code>Appointment</code>s from it
 */
public class TextParser implements AppointmentBookParser {
    private List<String> lines;

    /**
     * Constructor of class <code>TextParser</code>, reads in all the
     * lines from the file specified by parameter <code>filePath</code>
     *
     * @param filePath  the path of a text file to be parsed as a <code>String</code>
     * @throws IOException  if file does not exist or fail to read
     */
    public TextParser(String filePath) throws IOException {
        // read in text file
        lines = Files.readAllLines(Paths.get(filePath));
    }

    /**
     * This method parses a list of <code>String</code>s read from a text file
     * and creates an <code>AppointmentBook</code> accordingly
     *
     * @return an <code>AppointmentBook</code>
     * @throws ParserException if the text file is malformed
     */
    @Override
    public AbstractAppointmentBook<Appointment> parse() throws ParserException {
        // validate the text is well-formed
        int lineNum = lines.size();
        if (lineNum % 3 != 1) {
            throw new ParserException("Text file parsing error!");
        }

        // create an appointment book from it
        AppointmentBook apptbook = new AppointmentBook(lines.get(0));

        // adds appointments to appointment book
        if (lineNum > 1) {
            for (int i = 2; i < lineNum; i += 3) {
                String description = lines.get(i - 1);
                String beginDate = lines.get(i);
                String endDate = lines.get(i + 1);

                Date beginDateTime;
                Date endDateTime;
                try {
                    beginDateTime = parseDateTime(beginDate);
                    endDateTime = parseDateTime(endDate);
                } catch (IllegalArgumentException e) {
                    throw new ParserException("Invalid Date or Time format");
                }

                Appointment appt = new Appointment(description, beginDateTime, endDateTime);
                apptbook.addAppointment(appt);
            }
        }
        return apptbook;
    }
}
